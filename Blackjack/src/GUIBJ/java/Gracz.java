package GUIBJ.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Gracz {

    public List<CardType> karty = new ArrayList<CardType>();
    public List<CardType> zakryta = new ArrayList<CardType>();

    public void losujKarte(List<CardType> dostepne) {

        Random generator = new Random();
        int wylosowane = generator.nextInt(dostepne.size());
       // System.out.println("size: " +dostepne.size());
        //         System.out.println("wylosowane: "+wylosowane);
        this.karty.add(dostepne.get(wylosowane));
        dostepne.remove(wylosowane);
    }

    public void losujZakryta(List<CardType> dostepne) {

        Random generator = new Random();
        int wylosowane = generator.nextInt(dostepne.size());
       //  System.out.println("size: " +dostepne.size());
        // System.out.println("wylosowane: "+wylosowane);
        this.zakryta.add(dostepne.get(wylosowane));
        dostepne.remove(wylosowane);
    }

    public int wyliczSume() {
        int suma = 0;
        int limit = this.karty.size();
        for (int i = 0; i < limit; i++) {
            int wartoscKarty = this.karty.get(i).ordinal() / 4 + 2;
            if (wartoscKarty > 10 && wartoscKarty < 14) {
                wartoscKarty = 10;
            } else if (wartoscKarty == 14) {
                wartoscKarty = 11;
            }

            suma += wartoscKarty;
        }
        return suma;
    }

    public void zamienKarty() {
        this.karty.add(0, this.zakryta.get(0));
        this.zakryta.clear();
    }

}
